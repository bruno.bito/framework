<html>
    <body>
        <h1>VISUALIZAR ALUNOS</h1>
        <form method="post" action="{{route('alunos.atualizar')}}">
            @csrf

            <div>
                <label for="id">ID:</label>
                <input type="text" name="id" value="{{$aluno->id}}">
            </div>
            <div>
                <label for="nome">Nome:</label>
                <input type="text" name="nome" value="{{$aluno->nome}}" required>
            </div>

            <div>
                <label for="data_nascimento">Data de nascimento:</label>
                <input type="date" name="data_nascimento" value="{{$aluno->data_nascimento}}" required>
            </div>

            <div>
                <label for="email">Email:</label>
                <input type="email" name="email" value="{{$aluno->email}}" required>
            </div>

            <div>
                <label for="curso">Curso:</label>
                <input type="text" name="curso" value="{{$aluno->curso}}" required>
            </div>

            <div>
                <button type="submit">Atualizar Aluno</button>
            </div>
        </form>
    </body>
</html>
